import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashMap<String,Integer> games = new HashMap<>();
        games.put("Mario Odyssey",50);
        games.put("Super Smash Bros. Ultimate",20);
        games.put("Luigi's Mansion 3",15);
        games.put("Pokemon Sword",30);
        games.put("Pokemon Shield",100);

        games.forEach((key,value)->{
            System.out.println(key+" has "+value+" stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key,value)->{
            if(value <= 30){
                topGames.add(key);
                System.out.println(key+" has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        Scanner inputScanner = new Scanner(System.in);
        boolean addItem = true;
        while(addItem){
            System.out.println("Would you like to add an item? Yes or No");
            String input = inputScanner.nextLine();
            switch (input){
                case "Yes":
                    System.out.println("Add the item name");
                    String itemName = inputScanner.nextLine();

                    System.out.println("Add the item stack");
                    int itemStock = inputScanner.nextInt();

                    games.put(itemName,itemStock);
                    addItem = false;
                    games.forEach((key,value)->{
                        System.out.println(key+" has "+value+" stocks left.");
                    });
                    break;
                case "No":
                    System.out.println("Thank you");
                    addItem = false;
                    break;
                default:
                    System.out.println("Input out of range. Please try again.");
            }
        }
    }
}